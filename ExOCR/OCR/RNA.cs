using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using RNA;
using AForge.Neuro;
using AForge.Neuro.Learning;
using System.IO;
using NAudio.Wave;

namespace OCR
{
    class RNA
    {
        #region Atributos e Poperties
        private ActivationNetwork neuralNet;
        private double error;
        public double Error
        {
            get { return error; }
        }
        private int epoch;
        public int Epoch
        {
            get { return epoch; }
        }

        #endregion

        #region Suporte ao Treinamento
        /// <summary>
        /// Converte uma imagem bin�ria em um vetor, onde branco fica com -0.5 e preto com 0.5
        /// </summary>
        /// <param name="b"></param>
        /// <returns></returns>
        private double[] ImgToVector(Bitmap b)
        {
            double[] ret = new double[b.Width * b.Height];
            int x = 0;
            for (int i = 0; i < b.Width; i++)
            {
                for (int j = 0; j < b.Height; j++)
                {
                    Color c = b.GetPixel(i, j);
                    if (c.A == 255)
                    {
                        ret[x++] = -0.5;
                    }
                    else
                    {
                        ret[x++] = 0.5;
                    }
                }
            }
            return ret;
        }


        /// <summary>
        /// Contr�i um exemplo de sa��a da rede
        /// </summary>
        /// <param name="PatternNumber"> O n�mero do padr�o </param>
        /// <returns> Um vetor onde todos os elementos s�o -0.5 exceto na posi��o indicada pelo n�mero do padr�o</returns>
        private double[] BuildOutput(int PatternNumber, int PatternCount)
        {
            double[] ret = new double[PatternCount];
            for (int i = 0; i < PatternCount; i++)
            {
                ret[i] = (i == PatternNumber ? 0.5 : -0.5);
            }
            return ret;
        }

        #endregion

        #region M�todos da RNA

        // Construtor - cria e treina a rede
        public RNA(int patternSize, int patternCount, Bitmap[] patternImages)
        {
            neuralNet = new ActivationNetwork(
                new BipolarSigmoidFunction(),
                patternSize, 10, 10, patternCount);
            neuralNet.Randomize();


            BackPropagationLearning bpl = new BackPropagationLearning(neuralNet);
            bpl.LearningRate = 0.2;

            double[][] entrada = new double[patternCount][];
            for (int i = 0; i < entrada.Length; i++)
                entrada[i] = ImgToVector(patternImages[i]);

            double[][] saida = new double[patternCount][];
            for (int i = 0; i < saida.Length; i++)
                saida[i] = BuildOutput(i, patternCount);

            epoch = 1;
            do
            {
                error = bpl.RunEpoch(entrada, saida);
                Console.WriteLine(error);
                epoch++;
            } while (error > 0.0001 && epoch < 10000);

        }
        //array com todas as notas que vou treinar
        public RNA(int patternSize, int patternCount, double[][] bytesAudio)
        {
            neuralNet = new ActivationNetwork(
               new BipolarSigmoidFunction(),
               patternSize, 8, 10,10, patternCount);
            neuralNet.Randomize();


            BackPropagationLearning bpl = new BackPropagationLearning(neuralNet);
            bpl.LearningRate = 0.35;

            double[][] entrada = new double[patternCount][];
            for (int i = 0; i < entrada.Length; i++)
                entrada[i] = bytesAudio[i];

            double[][] saida = new double[patternCount][];
            for (int i = 0; i < saida.Length; i++)
                saida[i] = BuildOutput(i, patternCount);

            epoch = 1;
            do
            {
                error = bpl.RunEpoch(entrada, saida);
                epoch++;
            } while (error > 0.0001 && epoch < 10000);
        }

        // Reconhece um padr�o informado e retorna o n�mero do padr�o reconhecido
        public int Recognize(Bitmap patternImage)
        {
            double[] s = neuralNet.Compute(ImgToVector(patternImage));

            int max = 0;
            for (int i = 1; i < s.Length; i++)
            {
                if (s[i] > s[max])
                    max = i;
            }

            return max;
        }

        public int ReconheceSom(Double[] audio)
        {
            double[] s = neuralNet.Compute(audio);

            int max = 0;
            for (int i = 1; i < s.Length; i++)
            {
                if (s[i] > s[max])
                    max = i;
            }

            return max;
        }

        #endregion

        //#region Audio 

        //public static void AudioToca()
        //{
        //    double[][] notas;
        //    notas = new double[7][];
        //    int length;
        //    string s = "do;re;mi;fa;sol;la;si";
        //    string[] split = s.Split(';');
        //    for (int i = 0; i < 7; i++)
        //    {
        //        notas[i] = AdicionaNota("../../Sons/"  + split[i] + ".wav");
        //    }

        //    notas[0][0] = notas[0][0];

        //    RNA r = new RNA(43696, 1, notas);
        //}

        //static double[] AdicionaNota(string filename)
        //{
        //    WaveFileReader reader = new WaveFileReader(filename);

        //    byte[] buffer = new byte[reader.Length];
        //    int read = reader.Read(buffer, 0, buffer.Length);
        //    var floatSamples = new double[43696];
        //    for (int sampleIndex = 0; sampleIndex < 43696; sampleIndex++)
        //    {
        //        var intSampleValue = BitConverter.ToInt16(buffer, sampleIndex * 2);
        //        if(sampleIndex< 43696)
        //        floatSamples[sampleIndex] = intSampleValue / 32768.0;
        //    }

        //    return floatSamples;
        //}

        //#endregion

    }
}
