﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RNA
{
    public class Neuron
    {
        private double [] input;

        public double [] Input
        {
            get { return input; }
            set { input = value; }
        }

        
        private double[] synapses;

        public double[] Synapses
        {
            get { return synapses; }
            set { synapses = value; }
        }
        private double output;

        public double Output
        {
            get { return output; }
        }
        private double sigma;

        public double Sigma
        {
            get { return sigma; }
            set { sigma = value; }
        }

        public Neuron(int inputNumber)
        {
            Random rand = new Random();
            synapses = new double[inputNumber+1];
            for (int i = 0; i <= inputNumber; i++)
            {
                synapses[i] = rand.NextDouble() - 0.5;
            }
        }

        public void Evalute(double [] input)
        {
            if (input.Length != synapses.Length)
                throw new Exception("Tamanho da entrada inválido!");

            double vk = 0;

            for (int i = 0; i < input.Length; i++)
            {
                vk += input[i] * synapses[i];
            }

            this.input = (double[]) input.Clone();
            output = 1 / (1 + Math.Exp(-0.5*vk));
        }

        public double FirstDerivative()
        {
            return 0.5*output * (1 - output);
        }

    }
}
