using NAudio.Wave;
using RNA;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Media;
using System.Text;
using System.Windows.Forms;


namespace OCR
{
    public partial class Principal : Form
    {
        #region Atributos 
        private bool drawing;
        private RNA rnaOCR;
        private RNA r;
        #endregion

        #region Inicializa��o
        public Principal()
        {
            InitializeComponent();

            string s = "do;re;mi;fa;sol;la;si";
            string[] split = s.Split(';');
            for (int i = 0; i < 7; i++)
            {
                lstTestes.Items.Add(split[i] + ".wav");
                lstTestes.Items.Add(split[i] + "-stretched.wav");
            }
            lstTestes.Items.Add("do-octave.wav");
            lstTestes.Items.Add("do-stretched-octave.wav");
            
            // Cria a imagem padr�o
            // pbScratch.Image = new Bitmap(200, 200);

            // Inicializa��o de vari�veis
            drawing = false;
        }
        #endregion

        #region Rotinas de Desenho 

       // Limpando Imagem
        private void btnClear_Click(object sender, EventArgs e)
        {
           // pbScratch.Image = new Bitmap(200, 200);
        }

        // Adicionando imagem na lista
        private void btnAdd_Click(object sender, EventArgs e)
        {
           // Bitmap b = pbScratch.Image as Bitmap;
           // ilSamples.Images.Add(b);
           // lvInputs.Items.Add(txtName.Text);
            //lvInputs.Items[lvInputs.Items.Count - 1].ImageIndex = ilSamples.Images.Count - 1;
        }

        //Come�a a desenhar
        private void pbScratch_MouseDown(object sender, MouseEventArgs e)
        {
            drawing = true;
        }

        //P�ra de desenhar
        private void pbScratch_MouseUp(object sender, MouseEventArgs e)
        {
            drawing = false;
        }

        // Desenha pois o mouse est�Eapertado
        private void pbScratch_MouseMove(object sender, MouseEventArgs e)
        {
            if (drawing)
            {
               // Graphics g = Graphics.FromImage(pbScratch.Image);
        //        g.FillRectangle(Brushes.Black, e.X - 20, e.Y - 20, 40, 40);
                //pbScratch.Refresh();
            }
        }
        #endregion

        #region Opera��o da Rede
        //Treinamento da rede
        private void btnTreinar_Click(object sender, EventArgs e)
        {
            //Vetor de imagens para a rede
            Bitmap[] lstBM = new Bitmap[ilSamples.Images.Count];
            int i = 0;
            foreach (Bitmap b in ilSamples.Images)
            {
                lstBM[i++] = b;
            }

           // Criando e treinando a rede
           rnaOCR = new RNA(ilSamples.ImageSize.Height * ilSamples.ImageSize.Width,
               lvInputs.Items.Count,
               lstBM);

//Mostrando resultado do treinamento

           lblErro.Text = rnaOCR.Error.ToString("#0.#000");
            lblEpoch.Text = rnaOCR.Epoch.ToString();
        }

        // Chamando a rotina para classificar um padr�o
        private void btnTestar_Click(object sender, EventArgs e)
        {
           // ilSamples.Images.Add(pbScratch.Image);
            Bitmap b = ilSamples.Images[ilSamples.Images.Count - 1] as Bitmap;

            int result = rnaOCR.Recognize(b);
            lblResult.Text = lvInputs.Items[result].Text;

            ilSamples.Images.RemoveAt(ilSamples.Images.Count - 1);

        }


        #endregion

        private void button1_Click(object sender, EventArgs e)
        {
            MLP mlp = new MLP(0.2, 2);

            mlp.AddLayer(3);
            mlp.AddLayer(2);

            mlp.layers[0][0].Synapses[0] = -0.1;
            mlp.layers[0][0].Synapses[1] = 0.2;
            mlp.layers[0][0].Synapses[2] = 0.2;

            mlp.layers[0][1].Synapses[0] = 0.3;
            mlp.layers[0][1].Synapses[1] = -0.1;
            mlp.layers[0][1].Synapses[2] = 0.3;

            mlp.layers[0][2].Synapses[0] = 0.1;
            mlp.layers[0][2].Synapses[1] = 0.1;
            mlp.layers[0][2].Synapses[2] = 0.9;

            mlp.layers[1][0].Synapses[0] = 0.2;
            mlp.layers[1][0].Synapses[1] = 0.1;
            mlp.layers[1][0].Synapses[2] = -0.1;
            mlp.layers[1][0].Synapses[3] = -0.1;

            mlp.layers[1][1].Synapses[0] = -0.1;
            mlp.layers[1][1].Synapses[1] = 0.5;
            mlp.layers[1][1].Synapses[2] = 0.2;
            mlp.layers[1][1].Synapses[3] = 1.1;

            double[] res = mlp.Evaluate(new double[] { 0.1, 0.7 });

            double[][] input = new double[1][];
            input[0] = new double[] { 0.1, 0.7 };
            double[][] output = new double[1][];
            output[0] = new double[] { 0.2, 1.0 };

            mlp.Backpropagation(input, output);
        }

        private void btnTrocarAudio_Click(object sender, EventArgs e)
        {
            lvInputs.Items.Clear();
            double[][] notas;
            notas = new double[7][];
            string s = "do;re;mi;fa;sol;la;si";
            string[] split = s.Split(';');
            for (int i = 0; i < 7; i++)
            {
                notas[i] = AdicionaNota("../../Sons/" + split[i] + ".wav");
                lvInputs.Items.Add(split[i]);
                lvInputs.Items[lvInputs.Items.Count - 1].ImageIndex = ilSamples.Images.Count - 1;
            }

            r = new RNA(43696, 7, notas);



           // Mostrando resultado do treinamento

            lblErro.Text = r.Error.ToString("#0.#000");
            lblEpoch.Text = r.Epoch.ToString();
        }
        #region Audio 

        static double[] AdicionaNota(string filename)
        {
            WaveFileReader reader = new WaveFileReader(filename);

            byte[] buffer = new byte[reader.Length];
            int read = reader.Read(buffer, 0, buffer.Length);
            var floatSamples = new double[43696];
            for (int sampleIndex = 0; sampleIndex < 43696; sampleIndex++)
            {
                var intSampleValue = BitConverter.ToInt16(buffer, sampleIndex * 2);
                if (sampleIndex < 43696)
                    floatSamples[sampleIndex] = intSampleValue / 32768.0;
            }

            return floatSamples;
        }

        #endregion

        private void button1_Click_1(object sender, EventArgs e)
        {
            try
            {
                string s = "../../Sons/" + lstTestes.SelectedItems[0].Text;
                double[] nota = AdicionaNota(s);
                int result = r.ReconheceSom(nota);
                lblResult.Text = lvInputs.Items[result].Text;
            }
            catch (Exception)
            {

                
            }
        }

        private void btnTocar_Click(object sender, EventArgs e)
        {
            try
            {
                string s = lstTestes.SelectedItems[0].Text;

                SoundPlayer simpleSound = new SoundPlayer("../../Sons/" + s);
                simpleSound.Play();
            }
            catch (Exception)
            {

                
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                string s = lvInputs.SelectedItems[0].Text;

                SoundPlayer simpleSound = new SoundPlayer("../../Sons/" + s + ".wav");
                simpleSound.Play();
            }
            catch (Exception)
            {
                
            }
        }
    }
}