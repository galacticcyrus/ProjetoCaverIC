﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RNA
{
    public class MLP
    {
        public List<List<Neuron>> layers;
        public double[] inputLayer;
        public double learningRate;

        public MLP(double lr, int inputSize)
        {
            learningRate = lr;
            inputLayer = new double[inputSize];
            layers = new List<List<Neuron>>();
        }

        public void AddLayer(int neuronNumber)
        {
            List<Neuron> layer = new List<Neuron>();
            
            int ns = 0;
            if (layers.Count == 0)
                ns = inputLayer.Length;
            else
                ns = layers[layers.Count - 1].Count;

            for (int i = 0; i < neuronNumber; i++)
            {
                layer.Add(new Neuron(ns));
            }

            layers.Add(layer);
        }

        public double [] Evaluate(double [] input)
        {
            for (int i = 0; i < input.Length; i++)
                inputLayer[i] = input[i];

            double[] sinais = new double[inputLayer.Length+1];
            sinais[0] = 1;
            for (int i = 0; i < input.Length; i++)
                sinais[i+1] = input[i];

            int ca = 0;
            foreach (List<Neuron> l in layers)
            {
                foreach (Neuron n in l)
                {
                    n.Evalute(sinais);
                    if (ca == 0)
                        Console.WriteLine("Neurônio Oculto:" + n.Output);
                    else
                        Console.WriteLine("Neurônio Saída:" + n.Output);
                }
                ca++;

                sinais = new double[l.Count+1];
                sinais[0] = 1;
                for (int i = 0; i < sinais.Length-1; i++)
                {
                    sinais[i + 1] = l[i].Output;
                }
            }

            return sinais;
        }

        public void Backpropagation(double[][] input, double[][] output)
        {
            for (int k = 0; k < 1; k++)
            {
                double error = 0;
                for (int i = 0; i < input.GetLength(0); i++)
                {
                    double[] _i = new double[input[i].Length];
                    double[] _o = new double[output[i].Length];

                    for (int j = 0; j < _i.Length; j++)
                        _i[j] = input[i][j];

                    for (int j = 0; j < _o.Length; j++)
                        _o[j] = output[i][j];

                    UpdateSynapses(_i, _o);

                    double[] res = Evaluate(_i);
                    for (int j = 0; j < _o.Length; j++)
                        error += output[i][j] - res[j + 1];
                }
                //Console.WriteLine(error);
            }
        }

        private void UpdateSynapses(double [] input, double [] output)
        {
            // Calculando para a ultima camada
            double[] saidaObtida = this.Evaluate(input);
            for (int i = 0; i < layers[layers.Count-1].Count; i++)
            {
                double erro = output[i] - saidaObtida[i + 1];
                layers[layers.Count - 1][i].Sigma = layers[layers.Count - 1][i].FirstDerivative() * erro;
                Console.WriteLine("Sigma Neurônio Saída:" + layers[layers.Count - 1][i].Sigma);
            }

            // Calculando para as camadas ocultas
            for (int i = layers.Count-2; i >= 0; i--)
            {
                List<Neuron> l = layers[i];

                for (int j = 0; j < l.Count; j++)
                {
                    Neuron n = l[j];
                    double s = 0;
                    for (int k = 0; k < layers[i+1].Count; k++)
                    {
                        s += layers[i + 1][k].Sigma * layers[i + 1][k].Synapses[j + 1];
                    }
                    n.Sigma = n.FirstDerivative() * s;
                    Console.WriteLine("Sigma Neurônio Oculto:" + n.Sigma);
                }
            }

            // Atualizando os pesos
            foreach (List<Neuron> ln in layers)
            {
                foreach (Neuron n in ln)
                {
                    for (int i = 0; i < n.Synapses.Length; i++)
                    {
                        n.Synapses[i] += learningRate * n.Sigma * n.Input[i];
                    }
                }
            }


        }

    }
}
